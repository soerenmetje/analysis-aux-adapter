# Analysis of Lightning Aux Adapters

I bought the [iskey AUX Adapter](https://www.amazon.de/Ladeanschluss-Audiokabel-Verbindung-Lautsprecher-Kopfh%C3%B6rerAdapter/dp/B08D8TCM25/?_encoding=UTF8&pd_rd_w=5G1Ed&pf_rd_p=4b8c28f2-43e2-4ab5-ac65-455df1c49c83&pf_rd_r=Y6MAJG4TGQDGQVZ1KZZ3&pd_rd_r=1419fa7d-bff5-4aaf-a7b1-445433f9f917&pd_rd_wg=PZwY7&ref_=pd_gw_ci_mcx_mr_hp_d)
on Amazon. When I heard some songs, I felt like the adapter sounds somehow crappy compared to what I am used to. 
I performed the following Analysis to gain some objective results that are not based on my subjective perception. 

Test Questions:

1. Is there any difference in sound quality between multiple adapters?
1. Is there any difference to the sound quality of the original song?

To answer these questions, I analysed the frequency spectrum of original sound, and the sound that is produced by the adapters.
In this analysis, I focused on the frequency spectrum and did not cover any aspects of the dynamic range.
## Tested Adapters
- [Apple AUX Adapter](https://www.apple.com/shop/product/MMX62AM/A/lightning-to-35-mm-headphone-jack-adapter)
- [Amazon iskey AUX Adapter](https://www.amazon.de/Ladeanschluss-Audiokabel-Verbindung-Lautsprecher-Kopfh%C3%B6rerAdapter/dp/B08D8TCM25/?_encoding=UTF8&pd_rd_w=5G1Ed&pf_rd_p=4b8c28f2-43e2-4ab5-ac65-455df1c49c83&pf_rd_r=Y6MAJG4TGQDGQVZ1KZZ3&pd_rd_r=1419fa7d-bff5-4aaf-a7b1-445433f9f917&pd_rd_wg=PZwY7&ref_=pd_gw_ci_mcx_mr_hp_d)

> UPDATE: Luckily, Amazon does not sell the [Amazon iskey AUX Adapter](https://www.amazon.de/Ladeanschluss-Audiokabel-Verbindung-Lautsprecher-Kopfh%C3%B6rerAdapter/dp/B08D8TCM25/?_encoding=UTF8&pd_rd_w=5G1Ed&pf_rd_p=4b8c28f2-43e2-4ab5-ac65-455df1c49c83&pf_rd_r=Y6MAJG4TGQDGQVZ1KZZ3&pd_rd_r=1419fa7d-bff5-4aaf-a7b1-445433f9f917&pd_rd_wg=PZwY7&ref_=pd_gw_ci_mcx_mr_hp_d) anymore. Downside: Therefore the link is invalid.

![Tested Adapters](adapter.jpeg)
## Test Setup

![Analysis Setup](Analysis Setup.jpg)

## Test Workflow

I performed following steps:

### Preparation
1. Build the test setup 
1. Create a new [Audacity](https://www.audacityteam.org/) project
   
### Recording
1. Set Input Device to ``Line-In`` in Audacity
1. Build test setup. Use ``Apple AUX Adapter`` as `Lightning to AUX Adapter`
1. Add new track and name it *Apple*
1. Record first 30 sec of the song [Cheat Codes - No Promises (feat. Demi Lovato)](https://open.spotify.com/track/1louJpMmzEicAn7lzDalPW?si=34b1bebfd01143cd) in Audacity
1. Build test setup. Use ``Amazon iskey AUX Adapter`` as `Lightning to AUX Adapter`
1. Add new track and name it *Amazon iskey*
1. Record first 30 sec of the song [Cheat Codes - No Promises (feat. Demi Lovato)](https://open.spotify.com/track/1louJpMmzEicAn7lzDalPW?si=34b1bebfd01143cd) in Audacity

1. Set Input Device to ``What You Hear`` in Audacity. (This is a feature of my sound card. You may not have this Input Device)
1. Add new track and name it *Original*
1. Record first 30 sec of the song [Cheat Codes - No Promises (feat. Demi Lovato)](https://open.spotify.com/track/1louJpMmzEicAn7lzDalPW?si=34b1bebfd01143cd) in Audacity

### Processing
1. Align all tracks to match each time point
1. Cut each track to a fixed length of 30 sec
1. Normalize each track
1. For each track create a spectrum analysis (size set to 4096) in Audacity
1. Export each spectrum analysis to a ``.txt`` file


### Analysis
1. Loaded the spectrum analysis ``.txt`` files
1. Plotted all spectrum analysis to one diagram

## Test Results

The following plots show the frequency spectrum. 
The colored area visualizes the difference of the level in dB to the original sound. Less difference is better.
![alt text](plots/spectrum_23_25k.png)
![alt text](plots/spectrum_1k_10k.png)
![alt text](plots/spectrum_10k_20k.png)

## Conclusion

Both Adapters high-cut the sound at ~16.5 kHz. 
The [Amazon iskey AUX Adapter](https://www.amazon.de/Ladeanschluss-Audiokabel-Verbindung-Lautsprecher-Kopfh%C3%B6rerAdapter/dp/B08D8TCM25/?_encoding=UTF8&pd_rd_w=5G1Ed&pf_rd_p=4b8c28f2-43e2-4ab5-ac65-455df1c49c83&pf_rd_r=Y6MAJG4TGQDGQVZ1KZZ3&pd_rd_r=1419fa7d-bff5-4aaf-a7b1-445433f9f917&pd_rd_wg=PZwY7&ref_=pd_gw_ci_mcx_mr_hp_d) lacks of level below 200 Hz. At 50 Hz the difference is ~8 dB. 
Therefore the [Amazon iskey AUX Adapter](https://www.amazon.de/Ladeanschluss-Audiokabel-Verbindung-Lautsprecher-Kopfh%C3%B6rerAdapter/dp/B08D8TCM25/?_encoding=UTF8&pd_rd_w=5G1Ed&pf_rd_p=4b8c28f2-43e2-4ab5-ac65-455df1c49c83&pf_rd_r=Y6MAJG4TGQDGQVZ1KZZ3&pd_rd_r=1419fa7d-bff5-4aaf-a7b1-445433f9f917&pd_rd_wg=PZwY7&ref_=pd_gw_ci_mcx_mr_hp_d) is sent back to Amazon.